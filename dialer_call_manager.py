import time
import json
import logging
import os
import signal
import sys
import traceback
import websocket
import requests
from ari_manager import ARI
from rabbitmq_manager import RabbitMQManager
from redis_manager import RedisManager

logging.basicConfig(stream=sys.stdout, level=logging.INFO)


class CallManager:
    """
    CallManager manages the interaction with ARI (Asterisk REST Interface)
    and handles various call events through WebSocket.
    """

    def __init__(self):
        """
        Initializes the CallManager with necessary components like 
        RabbitMQ and Redis managers,
        and sets up ARI for interaction with Asterisk.
        """
        self.bridge_id = None
        self.rabbitmq_manager = RabbitMQManager()
        self.redis_manager = RedisManager()
        self.ari = ARI()
        self.channel_id = None
        self.id_camp = None
        self.id_customer = None
        self.tel_customer = None
        self.queue_timeout = None
        self.channel_type = None
        self.channel_id_pstn = None
        self.id_bridge = None

    def client(self):
        """
        Sets up the WebSocket client to connect to ARI and handle events.

        Returns:
            websocket.WebSocketApp: Configured WebSocket client.
        """
        try:
            ari_client = websocket.WebSocketApp(
                f"ws://{self.ari.host}:{self.ari.port}/ari/events"
                f"?api_key={self.ari.user}:{self.ari.password}&app={ASTERISK_APP}",
                on_message=self.on_message,
                on_error=self.on_error,
                on_close=self.on_close
            )
            return ari_client
        except Exception as e:
            logging.error("Error setting up ARI client: %s", str(e))
            logging.error(traceback.format_exc())
            return None

    def on_message(self, ws, message):
        """
        Handles incoming messages from the WebSocket 
        and directs them to the appropriate handler.

        Args:
            ws (websocket.WebSocketApp): The WebSocket client instance.
            message (str): The received message.
        """
        event_to_dict = json.loads(message)
        event_type = event_to_dict.get('type', 'default')

        # Logging para depuración
        # logging.info("Received event: %s", pprint.pformat(event_to_dict))

        if event_type == 'StasisStart':
            self.handle_stasis_start(event_to_dict)
        elif event_type == 'StasisEnd':
            self.handle_stasis_end(event_to_dict)
        elif event_type == 'ChannelDtmfReceived':
            self.handle_channel_dtmf_received(event_to_dict)
        elif event_type == 'ChannelHangupRequest':
            self.handle_channel_hangup_request(event_to_dict)
        elif event_type == 'ChannelStateChange':
            self.handle_channel_state_change(event_to_dict)
        elif event_type == 'ChannelCreated':
            self.handle_channel_created(event_to_dict)
        elif event_type == 'ChannelDestroyed':
            self.handle_channel_destroyed(event_to_dict)
        elif event_type == 'BridgeCreated':
            self.handle_bridge_created(event_to_dict)
        elif event_type == 'BridgeDestroyed':
            self.handle_bridge_destroyed(event_to_dict)
        elif event_type == 'PlaybackStarted':
            self.handle_playback_started(event_to_dict)
        elif event_type == 'PlaybackFinished':
            self.handle_playback_finished(event_to_dict)
        elif event_type == 'Dial':
            self.handle_dial(event_to_dict)

    def on_error(self, ws, error):
        """
        Handles WebSocket errors.

        Args:
            ws (websocket.WebSocketApp): The WebSocket client instance.
            error (str): The error message.
        """
        logging.error("WebSocket Error: %s", error)

    def on_close(self, ws, close_status_code, close_msg):
        """
        Handles the closing of the WebSocket connection.

        Args:
            ws (websocket.WebSocketApp): The WebSocket client instance.
            close_status_code (int): The status code for the close.
            close_msg (str): The close message.
        """
        logging.info("WebSocket closed connection")

    def on_open(self, ws):
        """
        Handles the opening of the WebSocket connection.

        Args:
            ws (websocket.WebSocketApp): The WebSocket client instance.
        """
        logging.info("WebSocket connection opened")

    def handle_stasis_start(self, event):
        """
        Handles the StasisStart event from ARI.

        Args:
            event (dict): The event data.
        """
        try:
            logging.info("Stasis Start: %s", event)

            channel_data = self.extract_channel_data(event)
            if channel_data:
                self.channel_id = channel_data.get('channel_id')
                self.id_camp = channel_data.get('id_camp')
                self.id_customer = channel_data.get('id_customer')
                self.tel_customer = channel_data.get('tel_customer')
                self.queue_timeout = channel_data.get('queue_timeout') or 30
                self.channel_type = channel_data.get('channel_type')
                self.channel_id_pstn = channel_data.get('channel_id_pstn')
                self.id_bridge = channel_data.get('id_bridge')                

                # Chequeo si la llamada llega por from-pstn
                if self.channel_type == 'pstn_dialout':
                    logging.info(
                        "Received ***StasisStart PSTN*** with args: "
                        "id_camp=%s, id_customer=%s, tel_customer=%s, "
                        "channel_type: %s, queue_timeout: %s , channel_id: %s",
                        self.id_camp, self.id_customer, self.tel_customer, 
                        self.channel_type, self.queue_timeout, self.channel_id
                    )
                    self.handle_pstn_dialout()
                elif self.channel_type == 'agent_dialto':
                    logging.info(
                        "Received ***StasisStart AGENT*** with args:"
                        "id_camp=%s, id_customer=%s, tel_customer=%s,"
                        "channel_type: %s, pstn_id_channel=%s,"
                        "id_bridge=%s, channel_id=%s",
                        self.id_camp, self.id_customer, self.tel_customer,
                        self.channel_type, self.channel_id_pstn,
                        self.id_bridge, self.channel_id
                    )
                    self.handle_agent_dialto()
            else:
                logging.error("Failed to extract channel data")
        except Exception as e:
            logging.error("Error handling stasis start: %s", str(e))

    def handle_dial(self, event):
        """
        Handles the Dial event from ARI.

        Args:
            event (dict): The event data.
        """
        try:
            dialstatus = event.get('dialstatus')
            dialstring = event.get('dialstring', 'Unknown')

            # Realiza acciones basadas en el estado del dial
            if dialstatus == 'RINGING':
                logging.info("The %s is ringing.", dialstring)
            elif dialstatus == 'ANSWER':
                logging.info("The %s answered the call", dialstring)
            elif dialstatus == 'BUSY':
                logging.info("The %s is busy.", dialstring)
                logging.info(
                    "Call try to %s fail with %s",
                    dialstring, dialstatus)
            elif dialstatus == 'NOANSWER':
                logging.info("The %s did not answer the call.", dialstring)
            elif dialstatus == 'CANCEL':
                logging.info("The %s call was canceled.", dialstring)
            elif dialstatus == 'CONGESTION':
                logging.info("The %s is congested.", dialstring)
            elif dialstatus == 'CHANUNAVAIL':
                logging.info("The %s is unavailable.", dialstring)
            elif dialstatus == 'DONTCALL':
                logging.info("The Customer phone is set to not receive calls.")
            elif dialstatus == 'TORTURE':
                logging.info(
                    "The phone is set to play a torture message.")
            elif dialstatus == 'INVALIDARGS':
                logging.info("Invalid arguments were provided for the dial.")
            else:
                logging.info("Dial status: %s", dialstatus)
        except Exception as e:
            logging.error("Error handling dial event: %s", str(e))

    def handle_channel_hangup_request(self, event):
        """
        Handles the ChannelHangupRequest event from ARI.

        Args:
            event (dict): The event data.
        """
        # logging.info("Hangup event: %s", event)
        try:
            channel = event.get('channel', {})
            channel_id = channel.get('id')
            cause = event.get('cause')

            if cause is not None:
                logging.info(
                    "Channel %s hangup request with cause %s",
                    channel_id, cause)
                if channel_id == self.channel_id:
                    self.hangup_agent_channel()
                    self.delete_bridge()
                elif channel_id == self.channel_id_pstn:
                    self.hangup_pstn_channel()
                    self.delete_bridge()
            else:
                logging.info(
                    "Channel %s hangup request received but"
                    "no cause provided",
                    channel_id)
        except Exception as e:
            logging.error("Error handling hangup request: %s", str(e))

    def handle_channel_dtmf_received(self, event):
        """
        Handles the ChannelDtmfReceived event from ARI.

        Args:
            event (dict): The event data.
        """
        channel = event.get('channel', {})
        channel_id = channel.get('id')
        digit = event.get("digit")
        logging.info("DTMF recibido en el canal %s: %s", channel_id, digit)

    def handle_stasis_end(self, event):
        """
        Handles the StasisEnd event from ARI.

        Args:
            event (dict): The event data.
        """
        # logging.info("Stasis END event: %s", event)
        try:
            channel = event.get('channel', {})
            channel_id = channel.get('id')

            if channel_id == self.channel_id:
                logging.info(
                    "********************* Canal de AGENTE Hangup "
                    "*********************")
                self.hangup_agent_channel()
                self.delete_bridge()
            elif channel_id == self.channel_id_pstn:
                logging.info(
                    "********************* Canal PSTN Hangup "
                    "*********************")
                self.hangup_pstn_channel()
                self.delete_bridge()
        except Exception as e:
            logging.error("Error handling stasis end: %s", str(e))

    def handle_channel_state_change(self, event):
        """
        Handles the ChannelStateChange event from ARI.

        Args:
            event (dict): The event data.
        """
        try:
            channel = event.get('channel', {})
            channel_id = channel.get('id')
            channel_state = channel.get('state')
            logging.info(
                "Channel %s state changed to %s", 
                channel_id, channel_state
            )
            # Aquí puedes manejar los diferentes estados del canal:
            if channel_state == 'Ringing':
                logging.info("Channel %s is ringing", channel_id)
            elif channel_state == 'Busy':
                logging.info("Channel %s is busy", channel_id)
            elif channel_state == 'Up':
                logging.info("Channel %s is up", channel_id)
            elif channel_state == 'Down':
                logging.info("Channel %s is down", channel_id)
            # Añade otros estados según sea necesario
        except Exception as e:
            logging.error("Error handling channel state change: %s", str(e))

    def extract_channel_data(self, event):
        """
        Extracts data from the event related to the channel.

        Args:
            event (dict): The event data.

        Returns:
            dict: Extracted data including channel_id, phone_number, 
            id_camp, id_customer, tel_customer, etc.
        """
        try:
            channel = event.get('channel', {})
            channel_id = channel.get('id')
            dialplan = channel.get('dialplan', {})
            app_data = dialplan.get('app_data', '')

            # Inicializar las variables
            id_camp = None
            id_customer = None
            channel_type = None
            queue_timeout = None
            channel_id_pstn = None
            id_bridge = None
            phone_number = None

            # Separar los argumentos de app_data
            args = app_data.split(',')

            # Ensamblar las variables usando app_data
            for arg in args:
                key_value = arg.split(':')
                if len(key_value) == 2:
                    key, value = key_value
                    key = key.strip()
                    value = value.strip()

                    if key == 'id_camp':
                        id_camp = value
                    elif key == 'id_customer':
                        id_customer = value
                    elif key == 'tel_customer':
                        phone_number = value
                    elif key == 'queue_timeout':
                        queue_timeout = value
                    elif key == 'channel_type':
                        channel_type = value
                    elif key == 'channel_id_pstn':
                        channel_id_pstn = value
                    elif key == 'id_bridge':
                        id_bridge = value

            return {
                'channel_id': channel_id,
                'id_camp': id_camp,
                'id_customer': id_customer,
                'tel_customer': phone_number,
                'queue_timeout': queue_timeout,
                'channel_type': channel_type,
                'channel_id_pstn': channel_id_pstn,
                'id_bridge': id_bridge
            }
        except Exception as e:
            logging.error("Error extracting channel data: %s", str(e))
            return None

    def answer_channel(self, channel_id):
        """
        Continues the execution of the dialplan for a given channel.

        Args:
            channel_id (str): The ID of the channel to be continued.
        """
        try:
            bridge = self.ari.create_bridge()
            if bridge is not None and 'id' in bridge:
                self.bridge_id = bridge.get('id')
            else:
                logging.error(
                    "Failed to create bridge or 'id' not present"
                    "in the response.")
                return

            self.ari.answer(channel_id)
            self.ari.add_channel_to_bridge(self.bridge_id, channel_id)

            logging.info(
                "Canal %s fue atendido al bridge %s",
                channel_id, self.bridge_id)
        except Exception as e:
            logging.error(
                "Error al continuar el canal %s: %s", 
                channel_id, str(e))

    def handle_agent_dialto(self):
        """
        Handle the agent call to logic.
        """
        try:
            id_channel_agent = self.channel_id

            logging.info(" ************ Canal de AGENTE atendido ************")
            
            try:
                # Play beep on the agent channel
                self.ari.playback(id_channel_agent, 'beep')
            except Exception as e:
                logging.error(
                    "Failed to play beep on agent channel %s: %s", 
                    id_channel_agent, str(e)
                )
                logging.error(traceback.format_exc())
            
            try:
                # Stop MOH on PSTN channel
                self.ari.stop_moh(self.channel_id_pstn)

                # Bridge PSTN and agent channels                
                self.ari.add_channel_to_bridge(
                    self.id_bridge, id_channel_agent
                )
                self.ari.add_channel_to_bridge(
                    self.id_bridge, self.channel_id_pstn
                )

                # Start recording the bridge
                recording_name = f"diale_callrec_{self.id_bridge}"
                recording_format = "wav"
                self.ari.start_recording(
                    self.id_bridge, 
                    recording_name, 
                    recording_format,
                    beep=True
                )
                logging.info("Recording started on bridge %s with name %s",
                             self.id_bridge, recording_name)

                logging.info(
                    "Successfully added agent channel %s"
                    "& PSTN channel %s to bridge %s",
                    id_channel_agent, self.channel_id_pstn, self.id_bridge)

            except Exception as e:
                logging.error(
                    "Failed to add agent channel %s to bridge %s: %s",
                    id_channel_agent, self.id_bridge, str(e))
                logging.error(traceback.format_exc())

        except Exception as e:
            logging.error("An error occurred while handling agent dialto: %s",
                          str(e))
            logging.error(traceback.format_exc())

    def handle_pstn_dialout(self):
        """
        Handle the PSTN dialout logic.
        """
        logging.info("************ Canal de PSTN atendido ************")

        self.answer_channel(self.channel_id)

        queue_name = f"queue_camp_{self.id_camp}"
        message_dict = {
            'id_channel': self.channel_id,
            'id_camp': self.id_camp,
            'id_customer': self.id_customer,
            'tel_customer': self.tel_customer,
            'id_bridge': self.bridge_id
        }
        message_json = json.dumps(message_dict)
        self.ari.start_moh(self.channel_id)
        self.enqueue_message(queue_name, self.queue_timeout, message_json)

    def enqueue_message(self, queue_name, queue_timeout, message_json):
        """
        Enqueue a message in RabbitMQ.

        Args:
            queue_name (str): The name of the queue.
            queue_timeout (int): The timeout for the queue.
            message_json (str): The message to be enqueued in JSON format.
        """
        try:
            if not self.rabbitmq_manager.is_connected():
                logging.warning("Reconnecting to RabbitMQ...")
                self.rabbitmq_manager.connect()

            result = self.rabbitmq_manager.publish_message(
                queue_name,
                message_json,
                queue_timeout
            )

            if not result:
                raise Exception("Failed to publish message to RabbitMQ")

            logging.info(
                "Mensaje enviado a RabbitMQ ! Queue: %s, Message: %s",
                queue_name, message_json)
        except Exception as e:
            logging.error("Error enqueuing message for channel: %s", str(e))
            logging.error(traceback.format_exc())

    def hangup_agent_channel(self):
        """
        Hangs up the agent channel if it exists.
        """
        try:
            if self.channel_id_pstn:
                self.ari.hangup_channel(self.channel_id_pstn)
                logging.info("Agent channel %s colgado", self.channel_id_pstn)
        except Exception as e:
            logging.error(
                "Error al colgar el canal de agente %s: %s", 
                self.channel_id_pstn, str(e))

    def hangup_pstn_channel(self):
        """
        Hangs up the PSTN channel if it exists.
        """
        try:
            if self.channel_id:
                self.ari.hangup_channel(self.channel_id)
                logging.info("PSTN channel %s colgado", self.channel_id)
        except Exception as e:
            logging.error(
                "Error al colgar el canal PSTN %s: %s", 
                self.channel_id, str(e))

    def hangup_channel(self):
        """
        Hangs up a given channel.
        """
        success = self.ari.hangup_channel(self.channel_id)
        if success:
            logging.info("Canal %s colgado", self.channel_id)
        else:
            logging.error("Error al colgar el canal %s", self.channel_id)

    def delete_bridge(self):
        """
        Deletes the current bridge.
        """
        try:
            if self.bridge_id:
                self.ari.destroy_bridge(self.bridge_id)
                logging.info("Bridge %s eliminado", self.bridge_id)
        except Exception as e:
            logging.error(
                "Error al eliminar el bridge %s: %s",
                self.bridge_id, str(e))

    def handle_channel_created(self, event):
        logging.info("Handling ChannelCreated event:")

    def handle_channel_destroyed(self, event):
        logging.info("Handling ChannelDestroyed event")
        # : %s", json.dumps(event, indent=2))

    def handle_bridge_created(self, event):
        logging.info("Handling BridgeCreated event:")
        #  %s", json.dumps(event, indent=2))

    def handle_bridge_destroyed(self, event):
        logging.info("Handling BridgeDestroyed event:")
        # %s", json.dumps(event, indent=2))

    def handle_playback_started(self, event):
        logging.info("Handling PlaybackStarted event")

    def handle_playback_finished(self, event):
        logging.info("Handling PlaybackFinished event:")
        #%s", json.dumps(event, indent=2))


if __name__ == "__main__":
    ASTERISK_USER = os.getenv('ARI_USER', 'default_user')
    ASTERISK_PASS = os.getenv('ARI_PASS', 'default_pass')
    ASTERISK_HOST = os.getenv('ARI_HOST', 'asterisk')
    ASTERISK_PORT = os.getenv('ARI_PORT', '7088')
    ASTERISK_APP = 'call_manager'

    ari_client = ARI(
        user=ASTERISK_USER,
        password=ASTERISK_PASS,
        host=ASTERISK_HOST,
        port=ASTERISK_PORT
    )

    call_manager = CallManager()

    ws = call_manager.client()

    def signal_handler(signum, frame):
        logging.info("Signal received, closing connection")
        ws.close()

    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    ws.on_open = call_manager.on_open
    ws.run_forever()
