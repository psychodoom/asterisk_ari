import json
import logging
import os
import sys
import traceback
import signal
import random
import pika
import redis
import threading
from ari_manager import ARI
from redis_manager import RedisManager

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

redis_conn = redis.Redis(
    host=os.getenv('REDIS_HOST', 'redis'),
    port=int(os.getenv('REDIS_PORT', '6379')),
    db=0,
    decode_responses=True
)


class RabbitMQConsumer(threading.Thread):
    """
    RabbitMQConsumer consumes messages from RabbitMQ and processes the
    Asterisk channel accordingly.
    """

    def __init__(self, queue_name):
        super().__init__()
        self.ari = ARI(
            user=os.getenv('ARI_USER', 'default_user'),
            password=os.getenv('ARI_PASS', 'default_pass'),
            host=os.getenv('ARI_HOST', 'asterisk'),
            port=int(os.getenv('ARI_PORT', '7088')),
        )
        self.redis_manager = RedisManager()
        self.connection = None
        self.channel = None
        self.queue_name = queue_name

    def connect(self):
        """
        Connects to RabbitMQ and sets up the channel and queue.
        """
        try:
            rabbitmq_host = os.getenv('RABBITMQ_HOSTNAME', 'localhost')
            rabbitmq_port = int(os.getenv('RABBITMQ_PORT', '5672'))
            rabbitmq_user = os.getenv('RABBITMQ_USER', 'guest')
            rabbitmq_pass = os.getenv('RABBITMQ_PASS', 'guest')

            credentials = pika.PlainCredentials(rabbitmq_user, rabbitmq_pass)
            self.connection = pika.BlockingConnection(
                pika.ConnectionParameters(
                    host=rabbitmq_host, port=rabbitmq_port,
                    credentials=credentials))
            self.channel = self.connection.channel()
            self.channel.queue_declare(queue=self.queue_name, durable=True)
            self.channel.basic_qos(prefetch_count=1)
            self.channel.basic_consume(
                queue=self.queue_name, on_message_callback=self.on_message)
            logging.info(
                "Connected to RabbitMQ on %s:%s, queue: %s",
                rabbitmq_host, rabbitmq_port, self.queue_name)

        except pika.exceptions.AMQPConnectionError as e:
            logging.error("Error connecting to RabbitMQ: %s", str(e))
            logging.error(traceback.format_exc())

    def on_message(self, ch, method, properties, body):
        """
        Callback function to handle incoming messages from RabbitMQ.

        Args:
            ch (pika.Channel): The channel object.
            method (pika.spec.Basic.Deliver): The method frame.
            properties (pika.spec.BasicProperties): The properties.
            body (bytes): The message body.
        """
        try:
            message = json.loads(body)
            logging.info("Received message: %s", message)

            id_channel_external = message.get('id_channel')
            id_camp = message.get('id_camp')
            id_customer = message.get('id_customer')
            tel_number = message.get('tel_customer')
            id_bridge = message.get('id_bridge')

            if id_channel_external:
                self.process_channel(
                    id_channel_external, id_camp, id_customer,
                    tel_number, id_bridge)
            else:
                logging.error("No channel ID found in the message")

            ch.basic_ack(delivery_tag=method.delivery_tag)

        except Exception as e:
            logging.error("Error processing message: %s", str(e))
            logging.error(traceback.format_exc())
            ch.basic_nack(delivery_tag=method.delivery_tag)

    def process_channel(self, channel_id, id_camp, id_customer,
                        tel_number, id_bridge):
        """
        Processes the channel based on the information received.

        Args:
            channel_id (str): The ID of the PSTN channel.
            id_camp (str): The campaign ID.
            id_customer (str): The customer ID.
            tel_number (str): The customer telephone number.
            id_bridge (str): The bridge ID.
        """
        try:
            pstn_channel = self.ari.get_channel_details(channel_id)
            pstn_channel_state = pstn_channel['state']

            logging.info(
                "Processing channel %s for campaign %s customer %s "
                "phone %s bridge %s pstn_channel_state %s",
                channel_id, id_camp, id_customer, tel_number,
                id_bridge, pstn_channel_state)

            if pstn_channel_state != 'Up':
                logging.warning(
                    "Channel state is not 'Up'. Current state: %s",
                    pstn_channel_state)
                return

            id_agent = 1
            
            channel_type = 'agent_dialto'

            call_data = {
                'endpoint': 'PJSIP/1004',
                'callerId': '01177660010',
                'timeout': 15,
                'app': os.getenv('ASTERISK_APP', 'call_manager'),
                'appArgs': (f'id_camp: {id_camp}, id_customer: {id_customer}, '
                            f'tel_customer: {tel_number}, channel_type: {channel_type}, '
                            f'channel_id_pstn: {channel_id}, id_bridge: {id_bridge}, id_agent: {id_agent}'),
                'variables': {
                    'PJSIP_HEADER(add,Origin)': 'DIALER',
                    'PJSIP_HEADER(add,OMLCALLID)': f'{channel_id}',
                    'PJSIP_HEADER(add,IDCliente)': f'{id_customer}',
                    'PJSIP_HEADER(add,IDCamp)': f'{id_camp}',
                    'PJSIP_HEADER(add,OMLCAMPTYPE)': '2',
                    'PJSIP_HEADER(add,OMLCALLTYPEIDTYPE)': '2',
                    'PJSIP_HEADER(add,OMLOUTNUM)': f'{tel_number}',
                    'PJSIP_HEADER(add,OMLVIDEO)': '',
                    'PJSIP_HEADER(add,OMLSURVEY)': '',
                    'PJSIP_HEADER(add,OMLCAMPNAME)': 'campana_clonada_7_9',
                    'PJSIP_HEADER(add,Omlrecfilename)': 'call_rec'
                }
            }

            response = self.ari.originate_channel(
                endpoint=call_data['endpoint'],
                app=call_data['app'],
                callerId=call_data['callerId'],
                appArgs=call_data['appArgs'],
                variables=call_data['variables']
            )

            if response.status_code == 200:
                logging.info('Llamada generada exitosamente')
            else:
                logging.error('Error al generar la llamada: %s - %s', response.status_code, response.text)

        except Exception as e:
            logging.error("Error processing channel %s: %s", channel_id, str(e))
            logging.error(traceback.format_exc())

    def run(self):
        self.connect()
        self.start_consuming()

    def start_consuming(self):
        """
        Starts consuming messages from the queue.
        """
        if self.channel:
            logging.info("Starting to consume messages...")
            self.channel.start_consuming()

    def stop_consuming(self):
        """
        Stops consuming messages from the queue and closes the connection.
        """
        if self.channel:
            self.channel.stop_consuming()
        if self.connection:
            self.connection.close()
        logging.info("Stopped consuming messages and closed connection")

    def get_available_agent_for_campaign(self, id_campaign):
        """
        Retrieves an available agent for the given campaign.

        Args:
            id_campaign (str): The campaign ID.

        Returns:
            tuple: The SIP agent endpoint and agent ID.
        """
        agents_key = f"OML:CAMP:{id_campaign}:AGENTS"
        agents = redis_conn.smembers(agents_key)

        logging.info("Agents for %s: %s", agents_key, agents)

        if agents:
            ready_agents = []
            for agent_id in agents:
                agent_data = redis_conn.hgetall(f"OML:AGENT:{agent_id}")
                status = agent_data.get('STATUS', '')
                if status == 'READY':
                    sip = agent_data.get('SIP')
                    ready_agents.append((sip, agent_id))

            if ready_agents:
                selected_agent_sip, selected_agent_id = random.choice(ready_agents)
                return f"PJSIP/{selected_agent_sip}", selected_agent_id
            else:
                logging.warning("No ready agents available for campaign %s", id_campaign)
                return None, None
        else:
            logging.warning("No agents assigned for campaign %s", id_campaign)
            return None, None


if __name__ == "__main__":
    QUEUES = {
        'queue_camp_9': '9',
        'queue_camp_10': '10'
    }

    consumers = []
    for queue, description in QUEUES.items():
        consumer = RabbitMQConsumer(queue)
        consumer.start()
        consumers.append(consumer)

    def signal_handler(signum, frame):
        logging.info("Signal received, stopping consumers...")
        for consumer in consumers:
            consumer.stop_consuming()

    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    for consumer in consumers:
        consumer.join()
