import pika
import os
import time
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class RabbitMQManager:
    MAX_RETRIES = 3
    RETRY_DELAY = 5  

    def __init__(self):
        self.rabbitmq_host = os.getenv('RABBITMQ_HOSTNAME', 'rabbitmq')        
        self.connection = None
        self.channel = None
        self.connect()  # Esta función conectará y creará el canal

    def connect(self):
        for _ in range(self.MAX_RETRIES):
            try:
                connection_params = pika.ConnectionParameters(
                    host=self.rabbitmq_host,
                    heartbeat=300,  
                    blocked_connection_timeout=900
                )
                self.connection = pika.BlockingConnection(connection_params)
                self.channel = self.connection.channel()
                break
            except pika.exceptions.AMQPError as e:
                logger.warning(f"Error connecting to RabbitMQ: {e}. Retrying...")
                time.sleep(self.RETRY_DELAY)

    def is_connected(self):
        return self.connection and self.connection.is_open

    def publish_message(self, queue_name, message, timeout):
        try:
            logging.info("Attempting to publish message to queue %s", queue_name)
            logging.info("Message: %s", message)
            self.channel.basic_publish(
                exchange='',
                routing_key=queue_name,
                body=message,
                properties=pika.BasicProperties(
                    delivery_mode=2,  # Make message persistent
                )
            )
            logging.info("Successfully published message to queue %s", queue_name)
            return True
        except Exception as e:
            logging.error("Failed to publish message to RabbitMQ: %s", str(e))
            logging.error(traceback.format_exc())
            return False


    def close_connection(self):
        try:
            if self.connection and self.connection.is_open:
                self.connection.close()
        except Exception as e:
            logger.error(f"Error closing connection: {e}")
