import os
import re
import sys
import time
import datetime
import pytz
import logging
import pystrix
import threading
import redis
from socket import setdefaulttimeout
import psycopg2
from psycopg2 import sql
import json


def write_time_stderr(message):
        root_logger.error(message)


    # ---- OML call logger postgres reportes_app_llamadalog ----
    # ---- OML call logger postgres reportes_app_llamadalog ----
    def omni_logger_conf(self, agi, *args, **kwargs):
        POSTGRES_HOST = os.getenv('PGHOST')
        POSTGRES_PORT = os.getenv('PGPORT')
        POSTGRES_DB = os.getenv('PGDATABASE')
        POSTGRES_USER = os.getenv('PGUSER')
        POSTGRES_PASS = os.getenv('PGPASSWORD')

        conn = psycopg2.connect(
            host=POSTGRES_HOST,
            port=POSTGRES_PORT,
            dbname=POSTGRES_DB,
            user=POSTGRES_USER,
            password=POSTGRES_PASS
        )
        cursor = conn.cursor()

        arguments = args[0]
        
        tz_variable = os.environ.get('TZ')

        if len(arguments) < 14:
            self.write_time_stderr('Error: No se proporcionaron suficientes argumentos\n')
            return

        if tz_variable:
            local_tz = pytz.timezone(tz_variable)
        else:
            local_tz = pytz.timezone('UTC')

        campana_id, callid, agente_id, event, numero_marcado, contacto_id, tipo_llamada, \
        tipo_campana, bridge_wait_time, duracion_llamada, archivo_grabacion, agente_extra_id, \
        campana_extra_id, numero_extra = arguments

        now = datetime.datetime.now(local_tz)
        #now = datetime.datetime.utcnow()
        now_formatted = now.strftime("%Y-%m-%d %H:%M:%S")

        llamadalog_dict = {
            'time': now_formatted,
            'callid': callid,
            'campana_id': campana_id,
            'tipo_campana': tipo_campana,
            'tipo_llamada': tipo_llamada,
            'agente_id': agente_id,
            'event': event,
            'numero_marcado': numero_marcado,
            'contacto_id': contacto_id,
            'bridge_wait_time': bridge_wait_time,
            'duracion_llamada': duracion_llamada,
            'archivo_grabacion': archivo_grabacion, 
            'agente_extra_id': agente_extra_id,
            'campana_extra_id': campana_extra_id,
            'numero_extra': numero_extra
        }

        # Insert to Postgres for history KPIs
        insert_query = sql.SQL(
            'INSERT INTO reportes_app_llamadalog ({}) VALUES ({})'
        ).format(
            sql.SQL(',').join(map(sql.Identifier, llamadalog_dict.keys())),
            sql.SQL(',').join(map(sql.Placeholder, llamadalog_dict.keys()))
        )

        try:
            cursor.execute(insert_query, llamadalog_dict)
            conn.commit()
        except Exception as e:
            self.write_time_stderr(f'Error due to: {e}\n')
        finally:
            cursor.close()
            conn.close()

        redis_key_camp = f'OML:REALTIME:CAMP:{campana_id}'        
        field_campana = f'CALL_TYPE:{tipo_llamada}:{event}'
        self.event_camp_sum(redis_key_camp, field_campana)

        redis_key_agent = f'OML:REALTIME:AGENT:{agente_id}'
        field_agent = f'CALL_TYPE:{tipo_llamada}:{event}'
        self.event_agent_sum(redis_key_agent, field_agent, event)
        
    # Redis INCRDB 
    def event_camp_sum(self, redis_key, field):
        try:
            redis_connection = redis.Redis(
                host=os.getenv('REDIS_HOSTNAME'),
                port=6379,
                db=2,
                decode_responses=True
            )
            redis_connection.hincrby(redis_key, field, 1)
        except redis.exceptions.RedisError as e:
            print(f"Error al incrementar el valor en Redis: {e}")
        except Exception as ex:
            print(f"Error inesperado: {ex}")