FROM python:3.10-alpine as build

WORKDIR /app

COPY requirements.txt /app/
RUN pip install -r ./requirements.txt

FROM python:3.10-alpine as run

WORKDIR /app
COPY --from=build /app /app
COPY --from=build /usr/local /usr/local
COPY ari.py rabbitmq_manager.py redis_manager.py postgres_manager.py adapter.py consumer.py deliver.py /app/
#RUN chmod +x /app/app_queue.py /app/fastagi.py
