# mixmonitor.py
import logging

class MixMonitor:
    def __init__(self, ari_client):
        self.ari = ari_client
        self.logger = logging.getLogger(self.__class__.__name__)

    def start_recording(self, bridge_id, filename, format='wav', beep=False):
        try:
            self.ari.bridges.record(
                bridgeId=bridge_id,
                name=filename,
                format=format,
                beep=beep,
                ifExists='overwrite'
            )
        except Exception as e:
            self.logger.error("Failed to start recording: %s", str(e))
