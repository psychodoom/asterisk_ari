# dial_agent.py
import logging
import os

class DialAgent:
    def __init__(self, ari_client):
        self.ari = ari_client
        self.logger = logging.getLogger(self.__class__.__name__)

    def dial_agent(self, agent_endpoint, caller_id, timeout, campaign_id, customer_id, customer_phone, channel_type, channel_id_pstn, bridge_id, agent_id):
        headers = {
            'PJSIP_HEADER(add,Origin)': 'DIALER',
            'PJSIP_HEADER(add,OMLCALLID)': f'{channel_id_pstn}',
            'PJSIP_HEADER(add,IDCliente)': f'{customer_id}',
            'PJSIP_HEADER(add,IDCamp)': f'{campaign_id}',
            'PJSIP_HEADER(add,OMLCAMPTYPE)': '2',
            'PJSIP_HEADER(add,OMLCALLTYPEIDTYPE)': '2',
            'PJSIP_HEADER(add,OMLOUTNUM)': f'{customer_phone}',
            'PJSIP_HEADER(add,OMLVIDEO)': '',
            'PJSIP_HEADER(add,OMLSURVEY)': '',
            'PJSIP_HEADER(add,OMLCAMPNAME)': 'campana_clonada_7_9',
            'PJSIP_HEADER(add,Omlrecfilename)': 'call_rec'
        }

        app = os.getenv('ASTERISK_APP', 'call_manager')
        app_args = f'id_camp: {campaign_id}, id_customer: {customer_id}, tel_customer: {customer_phone}, channel_type: {channel_type}, channel_id_pstn: {channel_id_pstn}, id_bridge: {bridge_id}, id_agent: {agent_id}'

        try:
            response = self.ari.channels.originate(
                endpoint=f'PJSIP/{agent_endpoint}',
                callerId=caller_id,
                timeout=timeout,
                app=app,
                appArgs=app_args,
                variables=headers
            )
            if response.status_code == 200:
                self.logger.info('Llamada al agente generada exitosamente')
                return True
            else:
                self.logger.error('Error al generar la llamada al agente: %s - %s', response.status_code, response.text)
                return False
        except Exception as e:
            self.logger.error("Error al procesar la llamada al agente: %s", str(e))
            self.logger.error(traceback.format_exc())
            return False
