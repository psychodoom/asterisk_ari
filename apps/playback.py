# playback.py
import logging

class Playback:
    def __init__(self, ari_client):
        self.ari = ari_client
        self.logger = logging.getLogger(self.__class__.__name__)

    def play_media(self, channel_id, media):
        try:
            self.ari.channels.play(channelId=channel_id, media=media)
            self.logger.info("Playing media %s on channel %s.", media, channel_id)
        except Exception as e:
            self.logger.error("Failed to play media on channel %s: %s", channel_id, str(e))
