# hangup.py
import logging

class Hangup:
    def __init__(self, ari_client):
        self.ari = ari_client
        self.logger = logging.getLogger(self.__class__.__name__)

    def hangup_channel(self, channel_id):
        try:
            self.ari.channels.hangup(channelId=channel_id)
            self.logger.info("Channel %s hung up successfully.", channel_id)
        except Exception as e:
            self.logger.error("Failed to hang up channel %s: %s", channel_id, str(e))
