
# dial.py
import logging

class Dial:
    def __init__(self, ari_client):
        self.ari = ari_client
        self.logger = logging.getLogger(self.__class__.__name__)

    def make_call(self, endpoint, caller_id, timeout, app, app_args):
        try:
            response = self.ari.channels.originate(
                endpoint=endpoint,
                callerId=caller_id,
                timeout=timeout,
                app=app,
                appArgs=app_args
            )
            return response
        except Exception as e:
            self.logger.error("Failed to make call: %s", str(e))
            return None
