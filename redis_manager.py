import redis
import os
import logging

class RedisManager:
    def __init__(self, host=None, port=6379, db=0, decode_responses=True):
        self.redis_host = host or os.getenv('REDIS_HOSTNAME', 'localhost')
        self.redis_port = port
        self.redis_db = db
        self.decode_responses = decode_responses
        self.connection = self.connect()

    def connect(self):
        """Try connecting to Redis and return the connection. None if failed."""
        try:
            connection = redis.Redis(
                host=self.redis_host,
                port=self.redis_port,
                db=self.redis_db,
                decode_responses=self.decode_responses
            )
            connection.ping()
            return connection
        except redis.ConnectionError as e:
            logging.error(f"Error connecting to Redis: {e}")
            return None

    def reconnect(self):
        """Reconnect to Redis if connection is lost."""
        self.connection = self.connect()

    def get(self, key):
        try:
            return self.connection.get(key)
        except redis.RedisError as e:
            logging.error(f"Error getting value from Redis: {e}")
            self.reconnect()
            return None

    def get_id_by_phone(self, phone_number):    
        try:
            key = f"OML:TEL:{phone_number}:ID"
            id_value = self.get(key)
            
            logging.info(f"Obtained value: {id_value}")
            return id_value if id_value else None
            
        except redis.RedisError as e:
            logging.error(f"Error fetching ID for phone number {phone_number}: {e}")
            return None

    def get_hash_as_dict(self, key):
        try:
            redis_hash = self.connection.hgetall(key)
            return {k: v for k, v in redis_hash.items()}
        except redis.RedisError as e:
            logging.error(f"Error getting hash from Redis: {e}")
            self.reconnect()
            return None

