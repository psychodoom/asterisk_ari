import requests
import json
import sys
import os
from ari_manager import ARI  # Importar la clase ARI desde ari.py

ASTERISK_USER = os.getenv('ARI_USER', 'default_user')
ASTERISK_PASS = os.getenv('ARI_PASS', 'default_pass')
ASTERISK_HOST = os.getenv('ARI_HOST', 'asterisk')
ASTERISK_PORT = os.getenv('ARI_PORT', '7088')
ASTERISK_APP = os.getenv('ASTERISK_APP', 'call_manager')

# Verifica que se hayan proporcionado los argumentos necesarios
if len(sys.argv) != 6:
    print("Uso: python call_sender.py <NUMERO> <ID_CAMP> <ID_CUSTOMER> <QUEUE_TIMEOUT> <DIAL_TIMEOUT>")
    sys.exit(1)

# Obtiene los datos de los argumentos de la línea de comandos
tel_number = sys.argv[1]
id_camp = sys.argv[2]
id_customer = sys.argv[3]
queue_timeout = sys.argv[4]
dial_timeout = sys.argv[5]
channel_type = 'pstn_dialout'

# Crear una instancia de la clase ARI
ari = ARI(
    user=ASTERISK_USER,
    password=ASTERISK_PASS,
    host=ASTERISK_HOST,
    port=int(ASTERISK_PORT)
)

# Datos de la llamada
call_data = {
    'endpoint': f'PJSIP/{tel_number}@TroncalSIP0',
    'callerId': '01177660010',
    'timeout': int(dial_timeout),
    'app': ASTERISK_APP,
    'appArgs': f'id_camp: {id_camp}, id_customer: {id_customer}, tel_customer: {tel_number}, queue_timeout: {queue_timeout}, channel_type: {channel_type}'
}

# Realiza la solicitud para crear un nuevo canal (originate)
response = ari.originate_channel(
    endpoint=call_data['endpoint'],
    app=call_data['app'],
    callerId=call_data['callerId'],
    appArgs=call_data['appArgs']
)

# Verifica la respuesta de Asterisk
if response and isinstance(response, dict) and 'id' in response:
    print('Llamada generada exitosamente')
else:
    print(f'Error al generar la llamada: {response}')
